
using UnrealBuildTool;
using System.Collections.Generic;

public class TestingGroundTarget : TargetRules
{
	public TestingGroundTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("TestingGround");
	}
}
